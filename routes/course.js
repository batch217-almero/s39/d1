const express = require("express");
const router = express.Router();

const Course =  require("../models/Course.js");
const courseController =  require("../controllers/courseController.js");

const auth = require("../auth.js")



// ACTIVITY

// Routes go here
router.post("/create", auth.verify, (request, response) => {

	const isAdmin = auth.decode(request.headers.authorization).isAdmin; 

	courseController.addCourse(request.body, isAdmin)
	.then(resultFromController => {response.send(resultFromController)});
})


module.exports = router;