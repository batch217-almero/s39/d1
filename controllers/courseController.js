const Course = require("../models/Course.js");

module.exports.addCourse = (reqBody, isAdmin) => {

	if(isAdmin){

		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save()
		.then((newCourse, error) => {
			if(error){
				return false;
			}

			return true
		})

	}
	// To avoid errors when user is not admin, we should return a promise
	let message = Promise.resolve('User must be ADMIN to access this.');

	return message.then((value) => {
		return value;
	});

}

	